library(dplyr)

#### Load raw output from database queries ####
rkm_iconclass_object <- read.csv("data-raw/rkm_iconclass_object.csv", stringsAsFactors = FALSE, na.strings = "")
rkm_object_artist <- read.csv("data-raw/rkm_list_data.csv", stringsAsFactors = FALSE, na.strings = "")
rkm_artist_attributes <- read.csv("data-raw/rkm_artists.csv", stringsAsFactors = FALSE, na.strings = "")

save(rkm_iconclass_object, file = "data/rkm_iconclass_object.RData")
save(rkm_object_artist, file = "data/rkm_object_artist.RData")

#### Process database output into node and edge lists ####
library(lubridate)

# Date range to investigate
daterange <- 1500:1750

window_size <- 5

# Parse node birth/death dates
rkm_artist_attributes <- rkm_artist_attributes %>%
  filter(name != "anoniem") %>%
  mutate(
    birth_year = year(ymd(dateOfBirth, truncated = 2)),
    death_year = year(ymd(dateOfDeath, truncated = 2))) %>%
  rename(birth_place = placeOfBirth, death_place = placeOfDeath)
save(rkm_artist_attributes, file = "data/rkm_artist_attributes.RData")

# There are several terms used to designate that an artwork was done after the
# design of an artist, by a publisher, or by an engraver. Here, I categorize
# these into three major classes
naar <- c(
  "naar ontwerp van",
  "naar tekening van",
  "naar schilderij van",
  "naar schildering van",
  "naar eigen ontwerp van",
  "naar prent van",
  "naar beeld van",
  "ontwerper",
  "cartograaf",
  "tekenaar",
  "inspirator")

uitgever <- c(
  "drukker",
  "uitvoerder",
  "supervisie",
  "verkoper",
  "boekverkoper",
  "handelaar",
  "uitvoerder",
  "prentverkoper",
  "boekhandelaar")

prentmaker <- c(
  "prentmaker",
  "vervaardiger",
  "graveur",
  "bloksnijder")

rkm_object_artist$role_category[rkm_object_artist$maker_role %in% naar] <- "naar"
rkm_object_artist$role_category[rkm_object_artist$maker_role %in% uitgever] <- "uitgever"
rkm_object_artist$role_category[rkm_object_artist$maker_role %in% prentmaker] <- "prentmaker"

# Establish a logical vector denoting if a maker role qualification is
# sufficient to be included
preferred_qualifications <- c(
  "vermeld op object",
  "eigenhandig gesigneerd",
  "mogelijk",
  "toegeschreven aan",
  "kopie naar",
  "naar",
  "mogelijk kopie naar")

rkm_object_artist$qualified <- rkm_object_artist$qualification %in% preferred_qualifications | is.na(rkm_object_artist$qualification)
save(rkm_object_artist, file = "data/rkm_object_artist.RData")

# Generate edge list by self-joining a denormalized table of objects with one
# row per creator-object pariring
exclude_makers <- c("anoniem", "onbekend")
source_table <- rkm_object_artist %>%
  filter(!(maker %in% exclude_makers) & qualified == TRUE) %>%
  select(object_id, source = maker, source_role = role_category, date_start, date_end)
target_table <- rkm_object_artist %>%
  filter(!(maker %in% exclude_makers) & qualified == TRUE) %>%
  select(object_id, target = maker, target_role = role_category, date_start, date_end)

# Essentially self-join the table of creators, creating every permutation of
# makers for every object. Filter out relationships of one maker to themselves
rkm_print_edges <- source_table %>%
  left_join(target_table, by = c("object_id", "date_start", "date_end")) %>%
  filter(source != target)

preferred_ties <- c("uitgever", "naar", "prentmaker")
rkm_print_edges <- rkm_print_edges %>%
  filter(source_role %in% preferred_ties & target_role %in% preferred_ties)

# Join the birth and death date for the source and target artists of each edge,
# and create a "coexist" variable describing if artists were alive (and over 15
# years old) during the same period
rkm_print_edges <- rkm_print_edges %>%
  left_join(rkm_artist_attributes %>% select(name, source_birth = birth_year, source_death = death_year, source_nat = nationality), by = c("source" = "name")) %>%
  left_join(rkm_artist_attributes %>% select(name, target_birth = birth_year, target_death = death_year, target_nat = nationality), by = c("target" = "name"))

coexist <- Vectorize(function(a1b, a1d, a2b, a2d) {
  if(anyNA(c(a1b, a1d, a2b, a2d))) {
    return(FALSE)
  } else {
    return(any((a1b + 15):a1d %in% (a2b + 15):a2d))
  }
})

rkm_print_edges <- rkm_print_edges %>%
  mutate(coexist = coexist(source_birth, source_death, target_birth, target_death))

# Any edges where target and source COEXIST will be kept. For edges that don't
# coexist, artists cannot be sources, and printers cannot be sources for
# publishers.
rkm_print_edges <- rkm_print_edges %>%
  filter(
    (source_role == "prentmaker" & target_role == "naar") |
      (source_role == "uitgever" & target_role == "naar") |
      (source_role == "uitgever" & target_role == "prentmaker")) %>%
  # First and second columns must be the source and target, respectively
  select(source, target, everything())

save(rkm_print_edges, file = "data/rkm_print_edges.RData")

#### Create subgraphs ####
library(igraph)

# Get the node and edge lists for this window, where all edges are within the
# window, and all artists were born before the end of the window.
get_subgraph <- function(edges, nodes, index, alive_only = FALSE) {

  # Calculate start and end years
  start_year <- index - window_size
  end_year <- index + window_size

  # Find nodes and edges that ought to be active w/in the specified window.
  # Nodes must be at least 15yo by the end of the window (a generous estimate
  # for the youngest contributions of a given artist), while edges must be
  # artworks whose creation timespan intersects with the window, and whose
  # participants belong to the sub_node list

  if(alive_only) {
    sub_nodes <- nodes %>% filter((birth_year + 15) <= end_year & death_year >= start_year)
  } else {
    sub_nodes <- nodes %>% filter((birth_year + 15) <= end_year)
  }

  sub_edges <- edges %>%
    filter(
      date_start <= end_year &
        date_end >= start_year &
        source %in% sub_nodes$name &
        target %in% sub_nodes$name)

  # Generate igraph object from the edges and nodes data frames
  sub_graph <- graph.data.frame(sub_edges, directed = TRUE, vertices = sub_nodes)
  simp_sub_graph <- simplify(sub_graph, remove.multiple = TRUE)

  # Return a list containing the igraph object as well as the edges and nodes
  # data frames
  sub_object <- list("index" = index, "graph" = simp_sub_graph, "edges" = sub_edges, "nodes" = sub_nodes)
  return(sub_object)
}


# Create an empty list to hold the calculated subgraphs
all_subgraphs <- function(edges, nodes, role_types = c("uitgever", "naar", "prentmaker"), alive_only = FALSE) {
  spec_edges <- edges %>% filter(source_role %in% role_types & target_role %in% role_types)
  lapply(daterange, function(x) get_subgraph(edges = spec_edges, nodes = nodes, index = x, alive_only = alive_only))
}

#### NETWORK METRICS ####

# Statistics calculated on the entire graph at once
global_stats <- function(graph, edges, nodes, index) {

  sub_trends <- data.frame(year = c(index))
  sub_trends$num_artists[1] <- length(V(graph))
  sub_trends$num_edges[1] <- length(E(graph))
  sub_trends$transitivity_global[1] <- transitivity(graph, type="global")
  sub_trends$diameter[1] <- diameter(graph)
  sub_trends$density[1] <- graph.density(graph, loops=TRUE)
  sub_trends$avg_path_length[1] <- average.path.length(graph)

  return(sub_trends)
}

# Statistics calculated per the nationality of the artist
national_stats <- function(graph, edges, nodes, index) {

  # Count works by nationality (This can only be done from the original edge
  # list, as the igraph object is flattened, without individual object
  # attributes)
  natl_works <- edges %>%
    rename(nationality = source_nat) %>%
    group_by(nationality, object_id) %>%
    count(nationality) %>%
    select(nationality, num_works = n)

  # List the nationalities present in the current graph
  present_nationalities <- unique(nodes$nationality[!is.na(nodes$nationality)])

  national_artists <- function(nat) {
    V(graph)[!is.na(nationality) & nationality == nat]
  }

  non_national_artists <- function(nat) {
    V(graph)[!is.na(nationality) & nationality != nat]
  }

  # Calculates "national" subgraph by only including nodes of the given
  # nationality
  strict_nationality <- function(nat) {
    nat_nodes <- national_artists(nat)
    nat_graph <- induced.subgraph(graph, vids = nat_nodes)
    return(nat_graph)
  }

  # Calculates "national" subgraph by including any edges that have an artist
  # of the given nationality as either a source or a target
  proximal_nationality <- function(nat) {
    nat_nodes <- national_artists(nat)
    nat_edges <- E(graph)[inc(nat_nodes)]
    nat_graph <- subgraph.edges(graph, eids = nat_edges)
    return(nat_graph)
  }

  # Create lists of strict and proximal subgraphs for the present nationalities
  strict_groups <- lapply(present_nationalities, function(x) strict_nationality(x))
  proximal_groups <- lapply(present_nationalities, function(x) proximal_nationality(x))

  ### Calculate strict and proximal national statistics
  natl_stats <- data_frame(nationality = present_nationalities)

  # Number of artists (nodes)
  natl_stats$artists_strict <- vapply(strict_groups, function(x) vcount(x), FUN.VALUE = numeric(1))
  natl_stats$artists_proximal <- vapply(proximal_groups, function(x) vcount(x), FUN.VALUE = numeric(1))

  # Number of edges
  natl_stats$edges_strict <- vapply(strict_groups, function(x) ecount(x), FUN.VALUE = numeric(1))
  natl_stats$edges_proximal <- vapply(proximal_groups, function(x) ecount(x), FUN.VALUE = numeric(1))

  # Graph degree centrality
  natl_stats$nat_deg_cent_strict <- vapply(strict_groups, function(x) centralization.degree(x, normalized=TRUE, mode = "out")$centralization, FUN.VALUE = numeric(1))
  natl_stats$nat_deg_cent_proximal <- vapply(proximal_groups, function(x) centralization.degree(x, normalized = TRUE, mode = "out")$centralization, FUN.VALUE = numeric(1))

  # Graph transitivity
  natl_stats$transitivity_strict <- vapply(strict_groups, function(x) transitivity(x, type = "global"), FUN.VALUE = numeric(1))
  natl_stats$transitivity_proximal <- vapply(proximal_groups, function(x) transitivity(x, type = "global"), FUN.VALUE = numeric(1))

  # Graph density
  natl_stats$density_strict <- vapply(strict_groups, function(x) graph.density(x, loops = FALSE), FUN.VALUE = numeric(1))
  natl_stats$density_proximal <- vapply(proximal_groups, function(x) graph.density(x, loops = FALSE), FUN.VALUE = numeric(1))

  # Graph diameter
  natl_stats$diameter_strict <- vapply(strict_groups, function(x) diameter(x), FUN.VALUE = numeric(1))
  natl_stats$diameter_proximal <- vapply(proximal_groups, function(x) diameter(x), FUN.VALUE = numeric(1))

  # Graph avg path length
  natl_stats$avg_path_strict <- vapply(strict_groups, function(x) average.path.length(x), FUN.VALUE = numeric(1))
  natl_stats$avg_path_proximal <- vapply(proximal_groups, function(x) average.path.length(x), FUN.VALUE = numeric(1))


  # External-Internal Index
  # Function for calculating national-level EII for each nationality present
  calc_nat_ei <- function(nat) {

    # Get all vertices which have the same attribute values as val
    internal <- edges %>% filter(source_nat == nat & target_nat == nat) %>% select(source, target) %>% distinct()
    # Get all vertices which have different attribute values than val
    external <- edges %>% filter(source_nat == nat & target_nat != nat) %>% select(source, target) %>% distinct()

    # Count internal vs. external connecitons
    i <- nrow(internal)
    e <- nrow(external)
    return((e - i)/(e + i))
  }

  natl_stats$eii <- vapply(present_nationalities, function(x) calc_nat_ei(x), FUN.VALUE = numeric(1))

  # Join all statistics by nationality and add the year
  natl_stats %>% inner_join(natl_works, by = "nationality") %>% mutate(year = index)
}

# Statistics calculated between different nationalities
comb_ei <- function(graph, edges, nodes, index) {

  # Find all nationalities present within the graph, and create a data frame with all their combinations
  present_nationalities <- unique(nodes$nationality[!is.na(nodes$nationality)])
  nat_combos <- expand.grid(present_nationalities, present_nationalities) %>% rename(nat1 = Var1, nat2 = Var2)

  calc_combo_ei <- function(nat1, nat2) {

    # Find all edges originating in nodes belonging to nat1
    sub_edges <- edges %>% filter(source_nat == nat1)

    # Find ties from nat1 to nat1
    internal <- sub_edges %>% filter(target_nat == nat1) %>% select(source, target) %>% distinct()

    # Find ties from nat1 to ALL external nodes
    external <- sub_edges %>% filter(target_nat != nat1) %>% select(source, target) %>% distinct()

    # Find ties from nat1 to nat2
    spec_external <- sub_edges %>% filter(target_nat == nat2) %>% select(source, target) %>% distinct()

    # Count internal vs. external connecitons
    return(data_frame(internal = nrow(internal), external = nrow(external), spec_external = nrow(spec_external)))
  }

  # Merge the dataframes of every nationality combo into one dataframe, and
  # attach it to the nat_combos frame, adding a column for the year
  combo_frame <- mapply(function(x, y) calc_combo_ei(x, y), nat_combos$nat1, nat_combos$nat2, SIMPLIFY = FALSE) %>% bind_rows()
  nat_combos %>% bind_cols(combo_frame) %>% mutate(year = index)
}

individual_stats <- function(graph, edges, nodes, index, compute_ind_ei = TRUE) {

  names = V(graph)$name

  sub_ind_trends <- data_frame(name = names, year = index)

  # Calculate vectorized statistics
  sub_ind_trends$total_degree <- degree(graph, mode = "total", normalized = FALSE)
  sub_ind_trends$out_degree <- degree(graph, mode = "out", normalized = FALSE)
  sub_ind_trends$in_degree <- degree(graph, mode = "in", normalized = FALSE)
  sub_ind_trends$total_degree_norm <- degree(graph, mode = "total", normalized = TRUE)
  sub_ind_trends$out_degree_norm <- degree(graph, mode = "out", normalized = TRUE)
  sub_ind_trends$in_degree_norm <- degree(graph, mode = "in", normalized = TRUE)
  sub_ind_trends$cent_betweenness <- centralization.betweenness(graph, normalized = TRUE)$res
  #sub_ind_trends$eigen <- centralization.evcent(graph, normalized = TRUE, directed = TRUE)$vector

  # Get the individial group-external/group-internal index for all nodes
  calc_ind_ei <- function(node_id) {

    # Determine the nationalities of the given artist
    individual_nat <- nodes$nationality[nodes$name == node_id]

    # Find all edges for which the artist is a source
    ego_edges <- edges %>% filter(source == node_id)

    if(nrow(ego_edges) == 0)
      return(NA)

    # Find all edges internal to given artist's nationalities
    internal_edges <- ego_edges %>% filter(target_nat == individual_nat) %>% select(source, target) %>% distinct()

    # Find all edges external to given artist's nationalities
    external_edges <- ego_edges %>% filter(target_nat != individual_nat) %>% select(source, target) %>% distinct()

    # Calculate the group-external/group-internal index for this individual node
    i <- nrow(internal_edges)
    e <- nrow(external_edges)
    return((e - i)/(e + i))
  }

  if(compute_ind_ei)
    sub_ind_trends$eii <- vapply(names, FUN.VALUE = numeric(1), function(x) calc_ind_ei(x))

  return(sub_ind_trends)
}


# Defaults to 20 cores for largest Digital Ocean droplet.
library(foreach)
library(doParallel)
registerDoParallel(20)

all_analyses <- function(subgraphs, compute_ind_ei = FALSE) {
  print("Global trends...")
  global_trends <- (foreach(i = subgraphs) %dopar% global_stats(i$graph, i$edges, i$nodes, i$index)) %>% bind_rows()

  print("National trends...")
  nat_trends <- (foreach(i = subgraphs) %dopar% national_stats(i$graph, i$edges, i$nodes, i$index)) %>% bind_rows()

  print("Binational EI Index...")
  nat_combos <- (foreach(i = subgraphs) %dopar% comb_ei(i$graph, i$edges, i$nodes, i$index)) %>% bind_rows()

  print("Individual Trends...")
  ind_trends <- (foreach(i = subgraphs) %dopar% individual_stats(i$graph, i$edges, i$nodes, i$index, compute_ind_ei)) %>% bind_rows() %>%
    # Allow more easily searchable graphs by adding nationality information to the
    # individual trends (this is important for finding the top most central
    # artists of a given nationality, for example)
    inner_join(rkm_artist_attributes %>% select(name, birth_year, death_year), by = "name")

  return(list(global_trends = global_trends, nat_trends = nat_trends, nat_combos = nat_combos, ind_trends = ind_trends))
}

#### Run all types of network analyses ####
## VERY COMPUTATIONALLY INTENSIVE - CAN TAKE SEVERAL HOURS TO RUN EVERY TYPE ##
rkm_APrPu_all <- all_subgraphs(edges = rkm_print_edges, nodes = rkm_artist_attributes, role_types = c("naar", "prentmaker", "uitgever"), alive_only = FALSE) %>% all_analyses(compute_ind_ei = TRUE)
save(rkm_APrPu_all, file = "data/rkm_APrPu_all.RData")

rkm_APrPu_alive <- all_subgraphs(edges = rkm_print_edges, nodes = rkm_artist_attributes, role_types = c("naar", "prentmaker", "uitgever"), alive_only = TRUE) %>% all_analyses(compute_ind_ei = TRUE)
save(rkm_APrPu_alive, file = "data/rkm_APrPu_alive.RData")

rkm_PrPu_all <- all_subgraphs(edges = rkm_print_edges, nodes = rkm_artist_attributes, role_types = c("prentmaker", "uitgever"), alive_only = FALSE) %>% all_analyses(compute_ind_ei = TRUE)
save(rkm_PrPu_all, file = "data/rkm_PrPu_all.RData")

rkm_PrPu_alive <- all_subgraphs(edges = rkm_print_edges, nodes = rkm_artist_attributes, role_types = c("prentmaker", "uitgever"), alive_only = TRUE) %>% all_analyses(compute_ind_ei = TRUE)
save(rkm_PrPu_alive, file = "data/rkm_PrPu_alive.RData")

rkm_APr_all <- all_subgraphs(edges = rkm_print_edges, nodes = rkm_artist_attributes, role_types = c("naar", "prentmaker"), alive_only = FALSE) %>% all_analyses(compute_ind_ei = TRUE)
save(rkm_APr_all, file = "data/rkm_APr_all.RData")

rkm_APr_alive <- all_subgraphs(edges = rkm_print_edges, nodes = rkm_artist_attributes, role_types = c("naar", "prentmaker"), alive_only = TRUE) %>% all_analyses(compute_ind_ei = TRUE)
save(rkm_APr_alive, file = "data/rkm_APr_alive.RData")

rkm_APu_all <- all_subgraphs(edges = rkm_print_edges, nodes = rkm_artist_attributes, role_types = c("naar", "uitgever"), alive_only = FALSE) %>% all_analyses(compute_ind_ei = TRUE)
save(rkm_APu_all, file = "data/rkm_APu_all.RData")

rkm_APu_alive <- all_subgraphs(edges = rkm_print_edges, nodes = rkm_artist_attributes, role_types = c("naar", "uitgever"), alive_only = TRUE) %>% all_analyses(compute_ind_ei = TRUE)
save(rkm_APu_alive, file = "data/rkm_APu_alive.RData")
