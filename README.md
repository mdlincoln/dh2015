DH2015
======

The data and code to reproduce the plots for "Modelling the (Inter)National Printmaking Networks of Early Modern Europe", *Digital Humanities 2015*, Sydney.

All plots are reproduced in the vignette at `vignettes/dh2015.html`.
The network analysis code can be found at `data-raw/bm.R` and `data-raw/rkm.R`.

To build the package yourself, install [devtools](https://cran.r-project.org/package=devtools), and then build this package with its vignettes:

```R
require("devtools")
devtools::install_git("https://gitlab.com/mdlincoln/dh2015.git", build_vignettes = TRUE)
```

[Published abstract](http://dh2015.org/abstracts/xml/LINCOLN_Matthew_Modelling_the__Inter_National_Pri/LINCOLN_Matthew_Modelling_the__Inter_National_Printmaki.html)

---
[Matthew D. Lincoln](http://matthewlincoln.net) | University of Maryland, College Park
